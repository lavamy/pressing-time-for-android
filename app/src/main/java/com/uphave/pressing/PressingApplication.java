package com.uphave.pressing;

import android.app.Application;

public class PressingApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PressingView.initSounds(getBaseContext());
    }


}
