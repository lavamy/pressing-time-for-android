package com.uphave.pressing;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerView extends TextView {
    private String mPrefix;
    private Timer mTimer;
    private long mStartTime;

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPrefix = getContext().getResources().getString(R.string.pressing_time);
        setVisibility(GONE);
    }

    public void start() {
        mStartTime = new Date().getTime();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new UpdateViewTimer(), 0, 100);
    }

    public void stop() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }

    private class UpdateViewTimer extends TimerTask {

        @Override
        public void run() {
            post(new Runnable() {
                @Override
                public void run() {
                    long time = new Date().getTime() - mStartTime;
                    long minute = time / 1000 / 60;
                    long second = (time / 1000) % 60;
                    long milli = (time % 1000) / 10;
                    setText(String.format("%s: %02d:%02d:%02d", mPrefix, minute, second, milli));
                }
            });
        }
    }
}
