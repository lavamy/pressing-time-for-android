package com.uphave.pressing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PressingView extends View {
    private static final String TAG = PressingView.class.getSimpleName();
    private static SoundPool sSoundPool;
    private static int[] sSounds;
    private static Random mRandom;

    private int mNumWaves = 10;
    private EventListener mEventListener;
    private float mTouchRadius = 140;
    private int mWaveColor = Color.WHITE;
    private boolean mTouchingDown;
    private boolean mWaving;
    private float mWaveCenterX, mWaveCenterY;
    private float mWaveAlpha[];
    private float mWaveRadius[];
    private Paint mPaint;
    private int mPlayingSound;
    private Timer mTimer;
    private float mScaleSizeOfDevices;
    private boolean mPressed;

    public PressingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWave();
        initSounds(context);
    }

    private void initWave() {
        mWaveAlpha = new float[mNumWaves];
        mWaveRadius = new float[mNumWaves];

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(2.0f);
        mPaint.setColor(mWaveColor);
        mPaint.setTextSize(50f);
        mPaint.setTextAlign(Paint.Align.CENTER);

        for (int i = 0; i < mNumWaves; i++) {
            mWaveRadius[i] = mTouchRadius;
            mWaveAlpha[i] = 255;
        }
    }

    static void initSounds(Context context) {
        AudioAttributes attributes = new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED).setUsage(AudioAttributes.USAGE_GAME).build();

        sSoundPool = new SoundPool.Builder().setAudioAttributes(attributes).setMaxStreams(2).build();
        sSounds = new int[2];

        sSounds[0] = sSoundPool.load(context, R.raw.ding, 1);
        sSounds[1] = sSoundPool.load(context, R.raw.kick, 1);

        mRandom = new Random(new Date().getTime());
    }

    public int getNumWaves() {
        return mNumWaves;
    }

    public void setNumWaves(int numWaves) {
        mNumWaves = numWaves;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (!mTouchingDown) {

                    if (mEventListener != null) {
                        mEventListener.onTouch();
                    }

                    mTouchingDown = true;

                    float x = ev.getX(), y = ev.getY();

                    if ((mWaveCenterX - x) * (mWaveCenterX - x) + (mWaveCenterY - y) * (mWaveCenterY - y) < mTouchRadius * mTouchRadius) {
                        startWave(x, y);

                        mPlayingSound = playSound(0, 1, true);
                        mPressed = true;

                        if (mEventListener != null) {
                            mEventListener.onPressed();
                        }
                    }
                    else {
                        mPlayingSound = playSound(1, 1, false);
                    }

                }
                return true;

            case MotionEvent.ACTION_MOVE:
                if (mPressed) {
                    float x = ev.getX(), y = ev.getY();

                    if ((mWaveCenterX - x) * (mWaveCenterX - x) + (mWaveCenterY - y) * (mWaveCenterY - y) >= mTouchRadius * mTouchRadius) {
                        mPressed = false;

                        stopSound(mPlayingSound);

                        stopWave();

                        mWaveCenterX = mRandom.nextInt(getWidth());
                        mWaveCenterY = mRandom.nextInt(getHeight());

                        if (mEventListener != null) {
                            mEventListener.onReleased();
                        }

                        invalidate();
                    }

                }

                return true;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (mTouchingDown) {
                    mTouchingDown = false;

                    if (mPressed) {
                        mPressed = false;

                        stopSound(mPlayingSound);

                        stopWave();

                        mWaveCenterX = mRandom.nextInt(getWidth());
                        mWaveCenterY = mRandom.nextInt(getHeight());

                        if (mEventListener != null) {
                            mEventListener.onReleased();
                        }

                        invalidate();
                    }
                }
                return true;
        }
        return false;
    }

    private void startWave(float x, float y) {
        mWaveCenterX = x;
        mWaveCenterY = y;
        initWave();
        mTimer = new Timer();

        mWaving = true;
        mTimer.scheduleAtFixedRate(new UpdateWaveTimerTask(), 0, 50);
    }

    private static int playSound(int soundID, float volume, boolean repeat) {
        if (sSoundPool == null || sSounds == null) {
            return -1;
        }

        return sSoundPool.play(sSounds[soundID], volume, volume, 1, repeat ? -1 : 0, 1f);
    }

    private static void stopSound(int soundID) {
        if (sSoundPool == null || sSounds == null) {
            return;
        }
        sSoundPool.stop(soundID);
    }

    private void stopWave() {
        if (mWaving) {
            mWaving = false;
            mTimer.cancel();
            mTimer = null;
            invalidate();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWaveCenterX = mTouchRadius + mRandom.nextInt(w - (int) (2 * mTouchRadius));
        mWaveCenterY = mTouchRadius + mRandom.nextInt(h - (int) (2 * mTouchRadius));

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setAlpha(255);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(mWaveCenterX, mWaveCenterY, mTouchRadius, mPaint);
        mPaint.setColor(Color.BLACK);
        canvas.drawText("Start", mWaveCenterX, mWaveCenterY, mPaint);

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mWaveColor);
        if (mWaving) {
            for (int i = 0; i < mNumWaves; i++) {
                mPaint.setAlpha((int) mWaveAlpha[i]);
                canvas.drawCircle(mWaveCenterX, mWaveCenterY, mWaveRadius[i], mPaint);
            }
        }
    }

    public EventListener getEventListener() {
        return mEventListener;
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    public float getTouchRadius() {
        return mTouchRadius;
    }

    public void setTouchRadius(float touchRadius) {
        mTouchRadius = touchRadius;
    }

    public int getWaveColor() {
        return mWaveColor;
    }

    public void setWaveColor(int waveColor) {
        mWaveColor = waveColor;
    }

    public interface EventListener {
        void onPressed();

        void onReleased();

        void onTouch();
    }

    private class UpdateWaveTimerTask extends TimerTask {

        @Override
        public void run() {
            post(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mNumWaves; i++) {
                        if (mWaveRadius[i] == mTouchRadius) {
                            // check whether initial wave
                            int previousWave = (i == 0) ? mNumWaves - 1 : i - 1;
                            if (i != 0 || mWaveRadius[previousWave] != mTouchRadius) {
                                if (mWaveRadius[previousWave] - mTouchRadius < mTouchRadius / 4) {
                                    continue;
                                }
                            }
                        }

                        mWaveRadius[i] *= 1.05f;
                        mWaveAlpha[i] -= 6f;

                        if (mWaveAlpha[i] < 0f) {
                            mWaveAlpha[i] = 255;
                            mWaveRadius[i] = mTouchRadius;
                        }
                    }

                    invalidate();
                }
            });
        }
    }
}
