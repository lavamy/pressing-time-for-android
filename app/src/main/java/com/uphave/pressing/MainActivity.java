package com.uphave.pressing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements PressingView.EventListener {

    private PressingView mPressingView;
    private TimerView mTimerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPressingView = (PressingView)findViewById(R.id.pressing);
        mTimerView = (TimerView)findViewById(R.id.timer);

        mPressingView.setEventListener(this);
    }

    @Override
    public void onPressed() {
        mTimerView.setVisibility(View.VISIBLE);
        mTimerView.start();
    }

    @Override
    public void onReleased() {
        mTimerView.stop();
    }

    @Override
    public void onTouch() {
        mTimerView.setVisibility(View.GONE);
    }
}
